**Automation API Testing Framework**


Overview

This repository contains an automation API testing framework built using Java and the Rest Assured library. The framework is designed to facilitate automated testing of REST APIs. It is structured into four main packages:

    Common Methods
    Runner
    Repository
    TestPackage

Package Breakdown
1. Common Methods

The common package includes utility and helper classes essential for executing API tests:

    API Trigger Methods: Contains methods for sending API requests and receiving responses.
    Utility Class: Provides utility functions such as reading data from Excel files and creating log files.

2. Runner

The runner package manages the execution of the test suite. It orchestrates the test runs and ensures that the tests are executed as intended.
3. Repository

The repository package is responsible for managing environmental variables and request bodies:

    Environmental Variables: Stores and provides access to environment-specific settings and credentials.
    Request Body: Manages the payloads used for various API requests.

4. TestPackage

The test package organizes and executes the actual test cases. It uses the methods provided in the common package and the configurations from the repository package to perform end-to-end API testing.
Getting Started
Prerequisites

    Java 11 or higher
    Maven (for dependency management)
    Rest Assured library
    Eclipse IDE

**Installation**


- [ ] Clone the Repository
- [ ] Navigate to the Project Directory
- [ ] Install Dependencies
- [ ] Set Up Environmental Variables
- [ ] Prepare Request Bodies
- [ ] Define your API request bodies in the repository package.
- [ ] run the test suite, using Maven command

Usage

    Adding New Tests: Create new test classes in the test package. Ensure they follow the naming conventions and utilize the methods from the common package.
    Updating Configuration: Modify the config.properties file and request body files as needed to match your testing environment and API requirements.
