Endpoint is :
https://reqres.in/api/unknown/3

Request body is :
/unknown/3

Response header date is : 
Sun, 25 Feb 2024 13:47:57 GMT

Response body is : 
{"data":{"id":3,"name":"true red","year":2002,"color":"#BF1932","pantone_value":"19-1664"},"support":{"url":"https://reqres.in/#support-heading","text":"To keep ReqRes free, contributions towards server costs are appreciated!"}}