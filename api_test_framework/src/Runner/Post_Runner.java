package Runner;

import java.io.IOException;

import TestPackage.Test_Case_1;
import TestPackage.tstlst;
import TestPackage.Test_Case_2;
import TestPackage.Test_Case_3;
import TestPackage.Test_Case_4;
import TestPackage.Test_Case_5;
import TestPackage.Test_Case_6;
import TestPackage.Test_Case_7;
import TestPackage.Test_Case_8;
import TestPackage.Test_Case_9;
import TestPackage.Test_Case_A0;
import TestPackage.Test_Case_A1;
import TestPackage.Test_Case_A2;
import TestPackage.Test_Case_A3;
import TestPackage.Test_Case_A4;
import TestPackage.Test_Case_A5;
import TestPackage.Test_Case_A6;
import TestPackage.Test_Case_A7;
import TestPackage.Test_Case_A8;



public class Post_Runner {
	public static void main(String[] args) throws ClassNotFoundException, IOException {
		Test_Case_1.executor();
		tstlst.count++;
		Test_Case_2.executor();
		tstlst.count++;
		Test_Case_3.executor();
		tstlst.count++;
		Test_Case_4.executor();
		tstlst.count++;
		Test_Case_5.executor();
		tstlst.count++;
		Test_Case_6.executor();
		tstlst.count++;
		Test_Case_7.executor();
		tstlst.count++;
		Test_Case_8.executor();
		tstlst.count++;
		Test_Case_9.executor();
		tstlst.count++;
		Test_Case_A0.executor();
		tstlst.count++;
		Test_Case_A1.executor();
		tstlst.count++;
		Test_Case_A2.executor();
		tstlst.count++;
		Test_Case_A3.executor();
		tstlst.count++;
		Test_Case_A4.executor();
		tstlst.count++;
		Test_Case_A5.executor();
		tstlst.count++;
		Test_Case_A6.executor();
		tstlst.count++;
		Test_Case_A7.executor();
		tstlst.count++;
		Test_Case_A8.executor();
		
	}

}