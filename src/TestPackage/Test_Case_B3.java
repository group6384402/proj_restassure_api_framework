package TestPackage;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test_Case_B3 extends RequestBody {
	
	public static void executor() throws ClassNotFoundException, IOException {
		
		File dir_name = Utility.CreateLogDirectory("Get_API_Logs");
		String requestBody = RequestBody.req_get_tc("Get_TC6");
		String Endpoint = Environment.Hostname() + Environment.Resource_get() + requestBody;
		int statuscode=0;

		for (int i = 0; i < 5; i++) {
			Response response = API_Trigger.Get_trigger(Endpoint);

			statuscode = response.statusCode();
			if (statuscode == 200) {
				Utility.evidenceFileCreator(Utility.testLogName(tstlst.Testname()), dir_name, Endpoint, requestBody,
						response.getHeader("Date"), response.getBody().asString());
				validator(response);
				break;
			}
			else {
				System.out.println("Expected status code is not found in current iteration :" +i+ " hence retrying");
			}

		}
		
		if (statuscode!=200) {
			System.out.println("Expected status code not found even after 5 retries hence failing the test case");
			Assert.assertEquals(statuscode, 200);
		}
		}
		public static void validator(Response response) {
			ResponseBody res_body = response.getBody();


		// Step 5.2 : Parse individual params using jsp_res object 
        String res_id=res_body.jsonPath().getString("data.id");
		String res_name=res_body.jsonPath().getString("data.name");
		String res_year=res_body.jsonPath().getString("data.year");
		String res_color=res_body.jsonPath().getString("data.color");
		String res_pantone_value=res_body.jsonPath().getString("data.pantone_value");
		
		
		//step  6.0 store expected result
		String exp_id="[1, 2, 3, 4, 5, 6]";
		String exp_name="[cerulean, fuchsia rose, true red, aqua sky, tigerlily, blue turquoise]";
		String exp_year="[2000, 2001, 2002, 2003, 2004, 2005]";
		String exp_color="[#98B2D1, #C74375, #BF1932, #7BC4C4, #E2583E, #53B0AE]";
		String exp_pantone_value="[15-4020, 17-2031, 19-1664, 14-4811, 17-1456, 15-5217]";
				 
		
		// Step 6 : Validate the response body

		// Step 6.2 : Use TestNG's Assert   
		Assert.assertEquals(res_id, exp_id);
		Assert.assertEquals(res_name,exp_name );
		Assert.assertEquals(res_year, exp_year);
		Assert.assertEquals(res_color, exp_color);
		Assert.assertEquals(res_pantone_value, exp_pantone_value);
	}


}


