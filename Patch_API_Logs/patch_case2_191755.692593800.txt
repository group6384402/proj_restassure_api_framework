Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "vinayak",
    "job": "qa"
}

Response header date is : 
Sun, 25 Feb 2024 13:47:55 GMT

Response body is : 
{"name":"vinayak","job":"qa","updatedAt":"2024-02-25T13:47:55.728Z"}